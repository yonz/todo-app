# Todo App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Running the app

Run `npm start` for a dev server. Then, navigate to `http://localhost:4200/`.

## Build

Run `npm build` to build the app.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Implementation Details

This app consists of a single module, with no routing, since this is an small app (for now).
The `TodosModule` consists of three components: `TasksListComponent`, `TaskComponent` and `NewTaskFormComponent`.

I used `ReactiveForms` to handle the changes on the main form and the edit form in each task. I also used [Bulma](https://bulma.io/) as CSS framework to make easier to layout and spend less time making things looks somewhat pretty.

Each task has an `edit` and `delete` button. The edit button will show the edit form which can be submitted on pressing `ENTER` or by clicking on the `save` button. Tasks can be completed by clicking/tapping on them.

![todo app in desktop](docs/desktop.png)
![todo app in mobile](docs/mobile.png)

I had to modify the API to fix two cases: enabling the endpoints to respond to CORS, and adding a `completed` field to the tasks.

For unit tests, I used [Testing Library](https://testing-library.com/), which provides a high level interface on top of TestBed to test the functionality of the components instead of the implementation details. This makes sure you are testing the ultimate outcome of the component.

![tests coverage](docs/coverage.png)
