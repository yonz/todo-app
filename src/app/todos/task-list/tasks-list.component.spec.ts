import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { render, RenderResult } from '@testing-library/angular';
import { of } from 'rxjs';
import { getTasks } from 'src/app/fixtures/tasks.fixture';
import { NewTaskFormComponent } from '../new-task-form/new-task-form.component';
import { TaskComponent } from '../task/task.component';
import { TasksService } from '../tasks.service';
import { TasksListComponent } from './tasks-list.component';

const tasks = getTasks();

describe('TasksListComponent', () => {
  let component: RenderResult<TasksListComponent>;

  beforeEach(async () => {
    component = await render(TasksListComponent, {
      imports: [ReactiveFormsModule, HttpClientTestingModule],
      providers: [{ provide: TasksService, useValue: tasksServiceMock() }],
      declarations: [TasksListComponent, TaskComponent, NewTaskFormComponent],
    });
  });

  it('renders', () => {
    expect(component).toBeTruthy();
  });

  it('renders a list of tasks', () => {
    const { queryAllByRole, queryByText } = component;
    const tasksService = TestBed.get(TasksService);
    expect(queryAllByRole('checkbox').length).toBe(3);

    tasks.forEach(task => {
      expect(queryByText(task.title)).toBeTruthy();
    });

    expect(tasksService.reset).toHaveBeenCalled();
  });

  describe('creating a task', () => {
    const title = 'new task';
    const description = 'new task description';

    beforeEach(() => {
      const { getByPlaceholderText, type, submit, getByRole } = component;

      type(getByPlaceholderText(/new task/i), title);
      type(getByPlaceholderText(/description/i), description);

      submit(getByRole('form'));
    });

    it('calls #create in the task service', () => {
      const tasksService = TestBed.get(TasksService);
      expect(tasksService.create).toHaveBeenCalledTimes(1);
      expect(tasksService.create).toHaveBeenCalledWith({
        title,
        description,
      });
    });
  });

  describe('editing a task', () => {
    const title = 'new task';
    const description = 'new task description';

    beforeEach(() => {
      const {
        queryAllByLabelText,
        getByLabelText,
        queryAllByRole,
        type,
        submit,
        click,
      } = component;

      const editButtons = queryAllByLabelText(/edit this task/i);

      click(editButtons[1]);
      type(getByLabelText(/task\'s title/i), title);
      type(getByLabelText(/task\'s description/i), description);

      submit(queryAllByRole('form')[1]);
    });

    it('calls #update in the task service', () => {
      const tasksService = TestBed.get(TasksService);

      expect(tasksService.update).toHaveBeenCalledTimes(1);
      expect(tasksService.update).toHaveBeenCalledWith(tasks[1].id, {
        title,
        description,
      });

      // firt time upon rendering, second time once the task is updated.
      expect(tasksService.reset).toHaveBeenCalledTimes(2);
    });
  });

  describe('deleting a task', () => {
    beforeEach(() => {
      const {
        queryAllByLabelText,
        getByLabelText,
        queryAllByRole,
        type,
        submit,
        click,
      } = component;

      const deleteButtons = queryAllByLabelText(/delete this task/i);

      click(deleteButtons[2]);
    });

    it('calls #delete in the task service', () => {
      const tasksService = TestBed.get(TasksService);

      expect(tasksService.delete).toHaveBeenCalledTimes(1);
      expect(tasksService.delete).toHaveBeenCalledWith(tasks[2].id);

      // firt time upon rendering, second time once the task is deleted.
      expect(tasksService.reset).toHaveBeenCalledTimes(2);
    });
  });
});

const tasksServiceMock = () => ({
  tasks$: of(tasks),
  reset: jasmine.createSpy(),
  create: jasmine.createSpy().and.returnValue(of(Promise.resolve())),
  update: jasmine.createSpy().and.returnValue(of(Promise.resolve())),
  delete: jasmine.createSpy().and.returnValue(of(Promise.resolve())),
});
