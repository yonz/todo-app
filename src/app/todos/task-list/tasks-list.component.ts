import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';
import { Observable } from 'rxjs';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
})
export class TasksListComponent implements OnInit {
  constructor(private tasks: TasksService) {}

  tasks$: Observable<Task[]> = this.tasks.tasks$;

  ngOnInit() {
    this.tasks.reset();
  }

  onCreate({ title, description }: { title: string; description: string }) {
    if (!title) {
      return;
    }

    this.tasks
      .create({
        title,
        description,
      })
      .subscribe();
  }

  onDelete(taskId: number) {
    this.tasks.delete(taskId).subscribe(() => {
      this.tasks.reset();
    });
  }

  onEdit({ taskId, changes }: { taskId: number; changes: Partial<Task> }) {
    this.tasks.update(taskId, changes).subscribe(() => {
      this.tasks.reset();
    });
  }

  trackByKey(index: number, task: Task) {
    return task.id;
  }
}
