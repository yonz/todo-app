import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TasksListComponent } from './task-list/tasks-list.component';
import { TasksService } from './tasks.service';
import { TaskComponent } from './task/task.component';
import { NewTaskFormComponent } from './new-task-form/new-task-form.component';

@NgModule({
  declarations: [TasksListComponent, TaskComponent, NewTaskFormComponent],
  providers: [TasksService],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [TasksListComponent],
})
export class TodosModule {}
