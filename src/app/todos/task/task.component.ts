import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {
  constructor(
    private changeDetector: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ) {}

  @Input()
  task: Task;

  @Output()
  delete: EventEmitter<number> = new EventEmitter();

  @Output()
  edit: EventEmitter<{
    taskId: number;
    changes: Partial<Task>;
  }> = new EventEmitter();

  @ViewChild('taskEditInput', { read: ElementRef, static: false })
  taskEditInput: ElementRef;

  private isEditing = new Subject();
  isEditing$ = this.isEditing.asObservable();

  form: FormGroup = this.formBuilder.group({
    title: [''],
    description: [''],
  });

  @HostBinding('class.completed')
  get completed(): boolean {
    return this.task && this.task.completed;
  }

  get taskLabelId(): string {
    if (this.task) {
      return 'task--' + this.task.id;
    }
  }
  onDeleteClick() {
    this.delete.emit(this.task.id);
  }

  onEditBtnClick() {
    this.isEditing.next(true);
    this.changeDetector.detectChanges();
    this.taskEditInput.nativeElement.focus();
  }

  onEditSubmit(evt: Event) {
    const values = this.form.value;

    evt.preventDefault();

    Object.assign(this.task, values);

    this.edit.emit({
      taskId: this.task.id,
      changes: values,
    });
    this.isEditing.next(false);
  }

  onComplete() {
    this.task.completed = !this.task.completed;
    this.edit.emit({
      taskId: this.task.id,
      changes: {
        completed: this.task.completed,
      },
    });
  }

  ngOnInit() {
    const { title, description } = this.task;
    this.form.setValue({ title, description });
  }
}
