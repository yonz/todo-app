import { ReactiveFormsModule } from '@angular/forms';
import { render, RenderResult } from '@testing-library/angular';
import { TaskComponent } from './task.component';
import { getTasks } from 'src/app/fixtures/tasks.fixture';

const tasks = getTasks();

describe('TaskComponent', () => {
  let component: RenderResult<TaskComponent>;

  const task = tasks[0];
  const deleteTask = jasmine.createSpy();
  const editTask = jasmine.createSpy();

  beforeEach(async () => {
    deleteTask.calls.reset();
    editTask.calls.reset();

    component = await render(TaskComponent, {
      imports: [ReactiveFormsModule],
      componentProperties: {
        task,
        delete: {
          emit: deleteTask,
        } as any,
        edit: {
          emit: editTask,
        } as any,
      },
    });
  });

  it('renders', () => {
    expect(component).toBeTruthy();
    expect(component.getByText(task.title)).toBeTruthy();
  });

  describe('clicking on delete', () => {
    it('emits the task id', () => {
      const { getByLabelText, click } = component;

      click(getByLabelText(/delete this task/i));

      expect(deleteTask).toHaveBeenCalledTimes(1);
      expect(deleteTask).toHaveBeenCalledWith(task.id);
    });
  });

  describe('clicking on the edit button', () => {
    beforeEach(() => {
      const { click, getByLabelText } = component;
      click(getByLabelText(/edit this task/i));
    });

    it('enters edit mode', () => {
      const { queryByRole, getByLabelText } = component;

      const form = queryByRole('form');
      const title = getByLabelText(/title/i);
      const description = getByLabelText(/description/i);

      expect(form).toBeTruthy();
      expect((<HTMLInputElement>title).value).toBe(task.title);
      expect((<HTMLTextAreaElement>description).value).toBe(task.description);
    });

    describe('and editing the task', () => {
      const newTitle = 'le new title';
      const newDescription = 'le new description';

      beforeEach(() => {
        const { getByRole, submit, type, getByLabelText } = component;

        type(getByLabelText(/title/i), newTitle);
        type(getByLabelText(/description/i), newDescription);
        submit(getByRole('form'));
      });

      it('emits the changes', () => {
        expect(editTask).toHaveBeenCalledTimes(1);
        expect(editTask).toHaveBeenCalledWith({
          taskId: task.id,
          changes: {
            title: newTitle,
            description: newDescription,
          },
        });
      });

      it('exits edit mode', () => {
        const { queryByLabelText } = component;

        expect(queryByLabelText(/save/i)).toBeNull();
        expect(queryByLabelText(/edit/i)).toBeTruthy();
      });

      it('shows the updated title', () => {
        const { queryByText } = component;

        expect(queryByText(newTitle)).toBeTruthy();
      });
    });
  });

  describe('completing a task', () => {
    it('emits the changes', () => {
      const { click, getByRole } = component;
      click(getByRole('checkbox'));

      expect(editTask).toHaveBeenCalledTimes(1);
      expect(editTask).toHaveBeenCalledWith({
        taskId: task.id,
        changes: {
          completed: true,
        },
      });

      editTask.calls.reset();

      click(getByRole('checkbox'));

      expect(editTask).toHaveBeenCalledTimes(1);
      expect(editTask).toHaveBeenCalledWith({
        taskId: task.id,
        changes: {
          completed: false,
        },
      });
    });
  });

  describe('deleting a task', () => {
    it('emits the changes', () => {
      const { click, getByLabelText } = component;
      click(getByLabelText(/delete/i));

      expect(deleteTask).toHaveBeenCalledTimes(1);
      expect(deleteTask).toHaveBeenCalledWith(task.id);
    });
  });
});
