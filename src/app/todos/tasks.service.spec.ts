import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { first, skip } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { getTasks } from '../fixtures/tasks.fixture';
import { Task } from '../models/task.model';
import { TasksService } from './tasks.service';

const tasks: Task[] = getTasks().map(task => new Task(task));

describe('TasksService', () => {
  let httpTestingController: HttpTestingController;
  let service: TasksService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TasksService],
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(TasksService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('#reset', () => {
    it('populates the list of tasks', () => {
      service.reset();

      service.tasks$
        .pipe(
          skip(1),
          first()
        )
        .subscribe(response => {
          expect(response.length).toEqual(tasks.length);
          expect(response[0]).toEqual(tasks[0]);
          expect(response[1]).toEqual(tasks[1]);
          expect(response[2]).toEqual(tasks[2]);
        });

      const req = httpTestingController.expectOne(
        `${environment.apiUrl}/tasks`
      );

      expect(req.request.method).toEqual('GET');

      req.flush(tasks);
    });
  });

  describe('#delete', () => {
    it('call the api to delete a task', () => {
      const id = tasks[1].id;
      service.delete(id).subscribe();

      const req = httpTestingController.expectOne(
        `${environment.apiUrl}/tasks/${id}`
      );

      expect(req.request.method).toEqual('DELETE');

      req.flush(tasks[1]);
    });
  });

  describe('#update', () => {
    it('call the api to update a task', () => {
      const id = tasks[0].id;
      const title = 'new task title';

      service
        .update(id, {
          title,
        })
        .subscribe();

      const req = httpTestingController.expectOne(
        `${environment.apiUrl}/tasks/${id}`
      );

      expect(req.request.method).toEqual('PATCH');
      expect(req.request.body).toEqual({ title });

      req.flush(tasks[1]);
    });
  });

  describe('#create', () => {
    it('call the api to create a task', () => {
      const title = 'new task title';
      const description = 'new task description';
      const body = {
        title,
        description,
      };

      service.tasks$
        .pipe(
          skip(1),
          first()
        )
        .subscribe(lTasks => {
          expect(lTasks[0].id).toEqual(100);
          expect(lTasks[0].title).toEqual(title);
          expect(lTasks[0].description).toEqual(description);
        });

      service.create(body).subscribe();

      const req = httpTestingController.expectOne(
        `${environment.apiUrl}/tasks`
      );

      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(body);

      req.flush({ ...body, id: 100 });
    });
  });
});
