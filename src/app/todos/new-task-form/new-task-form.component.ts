import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Task } from 'src/app/models/task.model';

@Component({
  selector: 'app-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.scss'],
})
export class NewTaskFormComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {}

  @Output()
  submit: EventEmitter<Partial<Task>> = new EventEmitter();

  form: FormGroup = this.formBuilder.group({
    title: [''],
    description: [''],
  });

  onSubmit(evt: Event) {
    evt.preventDefault();
    this.submit.emit(this.form.value);
    this.form.reset();
  }

  ngOnInit() {}
}
