import { ReactiveFormsModule } from '@angular/forms';
import { render, RenderResult } from '@testing-library/angular';
import { NewTaskFormComponent } from './new-task-form.component';

describe('NewTaskFormComponent', () => {
  let component: RenderResult<NewTaskFormComponent>;

  const submitTask = jasmine.createSpy();

  beforeEach(async () => {
    submitTask.calls.reset();

    component = await render(NewTaskFormComponent, {
      imports: [ReactiveFormsModule],
      componentProperties: {
        submit: {
          emit: submitTask,
        } as any,
      },
    });
  });

  it('renders', () => {
    expect(component).toBeTruthy();
    expect(component.getByPlaceholderText(/new task/i)).toBeTruthy();
  });

  describe('submitting the form', () => {
    const title = 'le title';
    const description = 'le descriptione';

    beforeEach(() => {
      const { type, getByPlaceholderText, getByRole, submit } = component;
      type(getByPlaceholderText(/new task/i), title);
      type(getByPlaceholderText(/description/i), description);
      submit(getByRole('form'));
    });

    it('emits the changes', () => {
      expect(submitTask).toHaveBeenCalledTimes(1);
      expect(submitTask).toHaveBeenCalledWith({
        title,
        description,
      });
    });
  });
});
