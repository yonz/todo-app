import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { first, map, tap, withLatestFrom } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Task } from '../models/task.model';

const API_URL = environment.apiUrl;

@Injectable()
export class TasksService {
  constructor(private http: HttpClient) {}

  private tasksSubject = new BehaviorSubject<Task[]>([]);
  public tasks$ = this.tasksSubject.asObservable();

  reset() {
    return this.http
      .get<Task[]>(`${API_URL}/tasks`)
      .pipe(
        map(tasks => tasks.map(task => new Task(task))),
        map(tasks =>
          tasks.sort((a, b) => Number(a.completed) - Number(b.completed))
        ),
        first()
      )
      .subscribe(tasks => this.tasksSubject.next(tasks));
  }

  create(data: Task) {
    return this.http.post<Task>(`${API_URL}/tasks`, data).pipe(
      map(task => new Task(task)),
      withLatestFrom(this.tasks$),
      tap(([task, tasks]) => {
        tasks.push(task);
        this.tasksSubject.next(tasks);
      }),
      first()
    );
  }

  delete(id: number) {
    return this.http.delete<Task>(`${API_URL}/tasks/${id}`).pipe(first());
  }

  update(id: number, changes: Partial<Task>) {
    return this.http
      .patch<Task>(`${API_URL}/tasks/${id}`, changes)
      .pipe(first());
  }
}
