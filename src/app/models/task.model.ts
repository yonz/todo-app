export class Task {
  title: string;
  completed?: boolean;
  id?: number;
  description?: string;
  priority?: number;
  createdAt?: number;
  updatedAt?: number;
  project?: any;

  constructor(props?) {
    Object.assign(this, props);
  }
}
