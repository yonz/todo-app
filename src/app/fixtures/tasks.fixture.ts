import { Task } from '../models/task.model';

export const getTasks = (): Task[] => {
  return [
    {
      completed: false,
      createdAt: 1570970796856,
      updatedAt: 1570970796856,
      id: 16,
      title: 'Create a to-do list app',
      description: 'Nothing fancy',
      priority: 0,
      project: null,
    },
    {
      completed: false,
      createdAt: 1570971017565,
      updatedAt: 1570971023825,
      id: 17,
      title: 'Go groceries shopping',
      description: '- Potatoes\n- Tomatoes\n- Milk\n- Bread and butter',
      priority: 0,
      project: null,
    },
    {
      completed: true,
      createdAt: 1570971102884,
      updatedAt: 1570971102884,
      id: 18,
      title: 'This is task with a long title and a description',
      description: 'Bla bla bla',
      priority: 0,
      project: null,
    },
  ];
};
